package com.example.midappone

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val helloButton: Button = findViewById(R.id.btn_hello)
        helloButton.setOnClickListener {
            val fullName:TextView = findViewById(R.id.Fullname)
            val stu_Id:TextView = findViewById(R.id.stuId)
            Log.d(TAG,"Fullname : ${fullName.text}  StudenId : ${stu_Id.text}")

            val inTent = Intent(this, HelloActivity::class.java)
            inTent.putExtra("fullname",fullName.text)
            startActivity(inTent)
        }
    }
}