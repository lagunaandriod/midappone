package com.example.midappone

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class HelloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)

        val showName: TextView = findViewById(R.id.txt_Fullname)
        val fullname: String = intent.getStringExtra("fullname").toString()
        showName.text = fullname
    }
}